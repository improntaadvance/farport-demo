export interface User {
  firstName: string;
  lastName: string;
  email: string;
}

export interface Address {
  street: string;
  zipCode: string;
  phoneNumber: string;
}
