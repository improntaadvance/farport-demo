import React, {createContext, useState, useContext, ReactChild} from 'react';
import {User} from '../models/user';

interface UserContextType {
  user?: User;
  setUser: (u?: User) => void;
}

const UserContext = createContext<UserContextType>({
  setUser: () => {},
});

export const UserProvider = ({children}: {children: ReactChild}) => {
  const [user, setUser] = useState<User>();

  return (
    <UserContext.Provider value={{user, setUser}}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser: () => UserContextType = () => useContext(UserContext);
