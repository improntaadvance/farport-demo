import {useState} from 'react';
import {Address as AddressType, User} from '../models/user';
import UserData from '../pages/Signup/UserData';
import Address from '../pages/Signup/Address';
import Password from '../pages/Signup/Password';
import Complete from '../pages/Signup/Complete';

export interface RegistrationRequestData extends User, AddressType {
  password: string;
}

export enum REGISTRATION_STEPS {
  USER_DATA,
  ADDRESS,
  PASSWORD,
  COMPLETE,
}

export const REGISTRATION: Record<
  REGISTRATION_STEPS,
  {
    component: Function;
    nextStep?: REGISTRATION_STEPS;
    prevStep?: REGISTRATION_STEPS;
  }
> = {
  [REGISTRATION_STEPS.USER_DATA]: {
    component: UserData,
    nextStep: REGISTRATION_STEPS.ADDRESS,
  },
  [REGISTRATION_STEPS.ADDRESS]: {
    component: Address,
    nextStep: REGISTRATION_STEPS.PASSWORD,
    prevStep: REGISTRATION_STEPS.USER_DATA,
  },
  [REGISTRATION_STEPS.PASSWORD]: {
    component: Password,
    prevStep: REGISTRATION_STEPS.ADDRESS,
    nextStep: REGISTRATION_STEPS.COMPLETE,
  },
  [REGISTRATION_STEPS.COMPLETE]: {
    component: Complete,
    prevStep: REGISTRATION_STEPS.PASSWORD,
  },
};

const useRegistrationRequest = () => {
  const [registrationData, setRegistrationData] = useState<
    RegistrationRequestData
  >({
    firstName: '',
    lastName: '',
    email: '',
    street: '',
    zipCode: '',
    phoneNumber: '',
    password: '',
  });

  const [currentStep, setCurrentStep] = useState<REGISTRATION_STEPS>(
    REGISTRATION_STEPS.USER_DATA
  );

  const handleUserData = (data: User) => {
    setRegistrationData({...registrationData, ...data});
    setCurrentStep(REGISTRATION_STEPS.ADDRESS);
  };

  const handleAddressData = (data: AddressType) => {
    setRegistrationData({...registrationData, ...data});
    setCurrentStep(REGISTRATION_STEPS.PASSWORD);
  };

  const handlePasswordData = (password: string) => {
    setRegistrationData({...registrationData, password});
    setCurrentStep(REGISTRATION_STEPS.COMPLETE);
  };

  const backToUserData = () => setCurrentStep(REGISTRATION_STEPS.USER_DATA);
  const backToAddress = () => setCurrentStep(REGISTRATION_STEPS.ADDRESS);

  return {
    registrationData,
    handleUserData,
    handleAddressData,
    handlePasswordData,
    backToUserData,
    backToAddress,
    currentStep,
  };
};

export default useRegistrationRequest;
