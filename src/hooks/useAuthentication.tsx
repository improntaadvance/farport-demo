import {useState, useEffect} from 'react';

import {getUser} from '../utils/api';
import {useUser} from '../contexts/userContext';

const useAuthentication = () => {
  const [isAuthenticating, setIsAuthenticating] = useState(true);
  const [isUserLogged, setIsUserLogged] = useState(false);
  const [isAuthenticationFailed, setIsAuthenticationFailed] = useState(false);

  const {user, setUser} = useUser();

  useEffect(() => {
    (async () => {
      try {
        setIsAuthenticating(true);
        const user = await getUser();

        if (user) {
          setIsUserLogged(true);
          setUser(user);
        }
      } catch (error) {
        setIsAuthenticationFailed(true);
      } finally {
        setIsAuthenticating(false);
      }
    })();
  }, [user, setUser]);

  return {isAuthenticating, isUserLogged, isAuthenticationFailed};
};

export default useAuthentication;
