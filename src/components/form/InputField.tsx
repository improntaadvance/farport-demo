import React from 'react';
import {useField} from 'react-final-form';

interface Props {
  name: string;
  label?: string;
  placeholder?: string;
  validate?: (value: string) => string | undefined;
}

const InputField: React.FC<Props> = (props) => {
  const {input, meta} = useField(props.name, {...props});

  const error = meta.touched && (meta.error || meta.submitError);

  return (
    <div>
      {props.label && <label htmlFor={props.name}>{props.label}</label>}
      <input {...input} />
      {error && <span>{error}</span>}
    </div>
  );
};

export default InputField;
