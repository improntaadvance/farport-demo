const createTestCafe = require('testcafe');

const config = {
  src: [`${__dirname}/pages`],
  browsers: ['chrome:headless'],
};

(async function start() {
  const testCafe = await createTestCafe('localhost');

  try {
    const runner = testCafe.createRunner();

    await runner
      .src(config.src)
      .browsers(config.browsers)
      .screenshots({
        path: `${__dirname}/__screenshots__/`,
        takeOnFails: true,
      })
      .run();
  } finally {
    await testCafe.close();
  }
})();
