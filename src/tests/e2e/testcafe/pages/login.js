import {waitForReact, ReactSelector} from 'testcafe-react-selectors';
import {getUrl} from '../utils';
import CONFIG from '../config';

fixture`Login page`.page`${CONFIG.baseUrl}/login`.beforeEach(async () => {
  await waitForReact();
});

test('Login page is shown', async (t) => {
  const button = ReactSelector('button').withText('Send');
  await t.expect(button.exists).ok();
});

test('Login button should not work until form is filled', async (t) => {
  const button = ReactSelector('button').withText('Send');

  await t.click(button);

  await t.wait(3000);

  await t.expect(getUrl()).contains('/login');
});

test('Login form should work', async (t) => {
  const button = ReactSelector('button').withText('Send');
  const email = ReactSelector('InputField')
    .withProps({name: 'email'})
    .find('input');
  const password = ReactSelector('InputField')
    .withProps({name: 'password'})
    .find('input');

  await t
    .typeText(email, 'email@test.com')
    .typeText(password, '123')
    .click(button);

  await t.wait(2000);

  const privateMessage = ReactSelector('p').withText('This is a private page');
  await t.expect(getUrl()).notContains('/login');
  await t.expect(privateMessage.exists).ok();
});
