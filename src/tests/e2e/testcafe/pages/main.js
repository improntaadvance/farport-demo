import {waitForReact, ReactSelector} from 'testcafe-react-selectors';
import {getUrl, UserRole} from '../utils';
import config from '../config';

fixture`Main page`.page`${config.baseUrl}/`.beforeEach(async () => {
  await waitForReact();
});

test('Should redirect to login page if not authenticated', async (t) => {
  await t.wait(2000);
  await t.expect(getUrl()).contains('/login');
});

test('Should be visible for authenticated user', async (t) => {
  await t.useRole(UserRole).navigateTo(`${config.baseUrl}/`);
  const privateMessage = ReactSelector('p').withText('This is a private page');
  await t.expect(getUrl()).notContains('/login');
  await t.expect(privateMessage.exists).ok();
});

test('Should contain a logout button', async (t) => {
  await t.useRole(UserRole).navigateTo(`${config.baseUrl}/`);
  const logout = ReactSelector('button').withText('Logout');
  await t.expect(logout.exists).ok();
});

test('Clicking on logout button should redirect to login page', async (t) => {
  await t.useRole(UserRole).navigateTo(`${config.baseUrl}/`);
  const logout = ReactSelector('button').withText('Logout');
  await t.click(logout);
  await t.expect(getUrl()).contains('/login');
});
