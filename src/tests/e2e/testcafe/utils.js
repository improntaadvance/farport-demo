import {ReactSelector, waitForReact} from 'testcafe-react-selectors';
import {Role, ClientFunction} from 'testcafe';
import CONFIG from './config';

export const getUrl = ClientFunction(() => window.location.href);

export const UserRole = Role(`${CONFIG.baseUrl}/login`, async (t) => {
  await waitForReact();

  const email = ReactSelector('InputField')
    .withProps({name: 'email'})
    .find('input');

  const password = ReactSelector('InputField')
    .withProps({name: 'password'})
    .find('input');

  const button = ReactSelector('button').withText('Send');

  await t
    .typeText(email, 'email@test.com')
    .typeText(password, '123')
    .click(button);

  await t.wait(2000);
});
