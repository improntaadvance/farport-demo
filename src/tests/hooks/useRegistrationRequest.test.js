import {renderHook, act} from '@testing-library/react-hooks';
import useRegistrationRequest, {
  REGISTRATION_STEPS,
} from '../../hooks/useRegistrationRequest';

test('initial registration step should be user data', () => {
  const {result} = renderHook(() => useRegistrationRequest());

  expect(result.current.currentStep).toBe(REGISTRATION_STEPS.USER_DATA);
});

test('handleUserData should go to address step and update registration user data', () => {
  const {result} = renderHook(() => useRegistrationRequest());
  const FIRSTNAME = 'John';
  act(() => {
    result.current.handleUserData({firstName: FIRSTNAME});
  });

  expect(result.current.currentStep).toBe(REGISTRATION_STEPS.ADDRESS);
  expect(result.current.registrationData.firstName).toBe(FIRSTNAME);
});

test('backToUserData should go to user data step and keep registration data', () => {
  const {result} = renderHook(() => useRegistrationRequest());
  const FIRSTNAME = 'John';
  act(() => {
    result.current.handleUserData({firstName: FIRSTNAME});
  });

  expect(result.current.currentStep).toBe(REGISTRATION_STEPS.ADDRESS);

  act(() => {
    result.current.backToUserData();
  });

  expect(result.current.registrationData.firstName).toBe(FIRSTNAME);
  expect(result.current.currentStep).toBe(REGISTRATION_STEPS.USER_DATA);
});

test('handlePasswordData should update password and go to complete step', () => {
  const {result} = renderHook(() => useRegistrationRequest());
  const PASSWORD = 'password';
  act(() => {
    result.current.handlePasswordData(PASSWORD);
  });
  expect(result.current.registrationData.password).toBe(PASSWORD);
  expect(result.current.currentStep).toBe(REGISTRATION_STEPS.COMPLETE);
});
