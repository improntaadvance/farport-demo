export const validatePassword = ({
  password,
  repeatPassword,
}: {
  password: string;
  repeatPassword: string;
}) => {
  if (password !== repeatPassword) {
    return {
      password: 'The two passwords do not match',
      repeatPassword: 'The two passwords do not match',
    };
  }
};

export const required = (value: string) =>
  !value ? 'This value is required' : undefined;
