const SESSION_TOKEN = 'farport-token';

export const saveToken = (token: string): void =>
  localStorage.setItem(SESSION_TOKEN, token);

export const getToken = (): string | null =>
  localStorage.getItem(SESSION_TOKEN);

export const clearToken = (): void => localStorage.removeItem(SESSION_TOKEN);
