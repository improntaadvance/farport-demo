import {User} from '../models/user';
import {clearToken, getToken, saveToken} from './storage';

const MOCK_USER: User = {
  firstName: 'Mock',
  lastName: 'User',
  email: 'mock@user.com',
};

// some fake logic to retrieve user
export const getUser = async () => {
  return await new Promise<User | null>((resolve) => {
    const token = getToken();
    if (token) {
      return resolve(MOCK_USER);
    }
    return resolve(null);
  });
};

export const login = async (email: string, password: string) => {
  // some fake logic to authenticate user
  return await new Promise<User>((resolve) => {
    saveToken(email);
    return resolve(MOCK_USER);
  });
};

export const logout = async () => {
  // some fake logic to logout user
  return await new Promise<void>((resolve) => {
    clearToken();
    return resolve();
  });
};
