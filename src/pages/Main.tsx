import React from 'react';
import {useHistory} from 'react-router-dom';

import {useUser} from '../contexts/userContext';
import {logout} from '../utils/api';

const Main = () => {
  const {user, setUser} = useUser();
  const history = useHistory();

  const handleLogout = async () => {
    await logout();
    history.replace('/login');
    setUser(undefined);
  };

  return (
    <div>
      <h2>{`Welcome ${user?.email}`}</h2>
      <p>This is a private page</p>
      <button onClick={handleLogout}>Logout</button>
    </div>
  );
};

export default Main;
