import React from 'react';
import {Form} from 'react-final-form';
import {Link, useHistory} from 'react-router-dom';

import InputField from '../components/form/InputField';
import {login} from '../utils/api';
import {required} from '../utils/validation';
import {useUser} from '../contexts/userContext';

const Login: React.FC = () => {
  const history = useHistory();
  const {setUser} = useUser();

  const onSubmit = async ({
    email,
    password,
  }: {
    email: string;
    password: string;
  }) => {
    const user = await login(email, password);
    setUser(user);
    history.push('/');
  };

  return (
    <div>
      <h2>Login to access private routes</h2>
      <Form onSubmit={onSubmit}>
        {({handleSubmit, invalid}) => (
          <form onSubmit={handleSubmit}>
            <InputField name="email" label="email" validate={required} />
            <InputField name="password" label="password" validate={required} />
            <button type="submit" disabled={invalid}>
              Send
            </button>
          </form>
        )}
      </Form>
      <h2>or</h2>
      <Link to="/signup">Signup</Link>
    </div>
  );
};

export default Login;
