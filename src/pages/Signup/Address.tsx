import React from 'react';
import {Form} from 'react-final-form';

import {RegistrationRequestData} from '../../hooks/useRegistrationRequest';
import InputField from '../../components/form/InputField';
import {Address as AddressData} from '../../models/user';
import {required} from '../../utils/validation';

interface Props {
  registrationData: RegistrationRequestData;
  onPrev: () => void;
  onNext: (data: AddressData) => void;
}

const Address: React.FC<Props> = ({registrationData, onNext, onPrev}) => {
  const onSubmit = (data: AddressData) => onNext(data);
  const handlePreviousStep = () => onPrev();

  return (
    <div>
      <h2>Please, fill address data</h2>
      <Form
        onSubmit={onSubmit}
        initialValues={{
          street: registrationData.street,
          zipCode: registrationData.zipCode,
          phoneNumber: registrationData.phoneNumber,
        }}
      >
        {({handleSubmit, invalid}) => (
          <form onSubmit={handleSubmit}>
            <InputField name="street" label="Street" validate={required} />
            <InputField name="zipCode" label="Zip Code" validate={required} />
            <InputField
              name="phoneNumber"
              label="Phone Number"
              validate={required}
            />
            <button type="button" onClick={handlePreviousStep}>
              Prev
            </button>
            <button type="submit" disabled={invalid}>
              Next
            </button>
          </form>
        )}
      </Form>
    </div>
  );
};

export default Address;
