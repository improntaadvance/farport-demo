import React from 'react';
import {Form} from 'react-final-form';

import {RegistrationRequestData} from '../../hooks/useRegistrationRequest';
import InputField from '../../components/form/InputField';
import {User} from '../../models/user';
import {required} from '../../utils/validation';

interface Props {
  registrationData: RegistrationRequestData;
  onNext: (data: User) => void;
}

const UserData: React.FC<Props> = ({registrationData, onNext}) => {
  const onSubmit = (data: User) => onNext(data);

  return (
    <div>
      <h2>Please, fill user data</h2>
      <Form
        onSubmit={onSubmit}
        initialValues={{
          firstName: registrationData.firstName,
          lastName: registrationData.lastName,
          email: registrationData.email,
        }}
      >
        {({handleSubmit, invalid}) => (
          <form onSubmit={handleSubmit}>
            <InputField
              name="firstName"
              label="Firstname"
              validate={required}
            />
            <InputField name="lastName" label="Lastname" validate={required} />
            <InputField name="email" label="Email" validate={required} />
            <button type="submit" disabled={invalid}>
              Next
            </button>
          </form>
        )}
      </Form>
    </div>
  );
};

export default UserData;
