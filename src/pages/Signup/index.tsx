import React from 'react';

import useRegistrationRequest, {
  REGISTRATION_STEPS,
} from '../../hooks/useRegistrationRequest';
import Address from './Address';
import Complete from './Complete';
import Password from './Password';
import UserData from './UserData';

const Signup = () => {
  const {registrationData, currentStep, ...actions} = useRegistrationRequest();

  if (currentStep === REGISTRATION_STEPS.USER_DATA) {
    return (
      <UserData
        registrationData={registrationData}
        onNext={actions.handleUserData}
      />
    );
  }

  if (currentStep === REGISTRATION_STEPS.ADDRESS) {
    return (
      <Address
        registrationData={registrationData}
        onNext={actions.handleAddressData}
        onPrev={actions.backToUserData}
      />
    );
  }

  if (currentStep === REGISTRATION_STEPS.PASSWORD) {
    return (
      <Password
        registrationData={registrationData}
        onNext={actions.handlePasswordData}
        onPrev={actions.backToAddress}
      />
    );
  }

  if (currentStep === REGISTRATION_STEPS.COMPLETE) {
    return <Complete />;
  }

  return null;
};

export default Signup;
