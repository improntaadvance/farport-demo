import React from 'react';
import {Form} from 'react-final-form';

import {RegistrationRequestData} from '../../hooks/useRegistrationRequest';
import InputField from '../../components/form/InputField';
import {validatePassword, required} from '../../utils/validation';

interface Props {
  registrationData: RegistrationRequestData;
  onPrev: () => void;
  onNext: (password: string) => void;
}

const Password: React.FC<Props> = ({registrationData, onNext, onPrev}) => {
  const onSubmit = (values: {password: string}) => onNext(values.password);
  const handlePreviousStep = () => onPrev();

  return (
    <div>
      <h2>Please, choose a password</h2>
      <Form
        onSubmit={onSubmit}
        validate={validatePassword}
        initialValues={{
          password: registrationData.password,
          repeatPassword: '',
        }}
      >
        {({handleSubmit, invalid}) => (
          <form onSubmit={handleSubmit}>
            <InputField name="password" label="Password" validate={required} />
            <InputField
              name="repeatPassword"
              label="Repeat Password"
              validate={required}
            />
            <div></div>
            <button type="button" onClick={handlePreviousStep}>
              Prev
            </button>
            <button type="submit" disabled={invalid}>
              Send
            </button>
          </form>
        )}
      </Form>
    </div>
  );
};

export default Password;
