import React from 'react';
import {Link} from 'react-router-dom';

const Complete: React.FC = () => {
  return (
    <div>
      <h2>Registration Completed</h2>
      <Link to="login">Login</Link>
    </div>
  );
};

export default Complete;
