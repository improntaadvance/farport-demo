import React, {useCallback} from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import './App.css';

import {UserProvider} from './contexts/userContext';
import useAuthentication from './hooks/useAuthentication';
import Login from './pages/Login';
import Main from './pages/Main';
import Signup from './pages/Signup';

function App() {
  const {isUserLogged, isAuthenticating} = useAuthentication();

  const PrivateRoute = useCallback(
    (props) =>
      isUserLogged ? (
        <Route {...props} />
      ) : (
        <Redirect path={props.path} exact={props.exact} to="/login" />
      ),
    [isUserLogged]
  );

  if (isAuthenticating) {
    return (
      <div>
        <h2>Loading</h2>
      </div>
    );
  }

  return (
    <div className="App">
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
        <PrivateRoute path="/" exact component={Main} />
      </Switch>
    </div>
  );
}

const withProviders = (Comp: React.FC) => (props: any) => (
  <UserProvider>
    <Router>
      <Comp {...props} />
    </Router>
  </UserProvider>
);

export default withProviders(App);
